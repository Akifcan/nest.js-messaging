import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, Min } from 'class-validator';

@InputType()
class LoginDto {
  @Field()
  @IsEmail()
  @IsNotEmpty()
  email: string;
  @Field()
  @IsNotEmpty()
  @Min(5)
  password: string;
}

export default LoginDto;
