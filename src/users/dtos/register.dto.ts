import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, Min } from 'class-validator';

@InputType()
class RegisterDto {
  @Field()
  @IsEmail()
  @IsNotEmpty()
  email: string;
  @Field()
  @IsNotEmpty()
  @Min(5)
  password: string;
  @Field()
  @IsNotEmpty()
  username: string;
}

export default RegisterDto;
