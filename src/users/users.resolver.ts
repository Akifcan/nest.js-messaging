import { Inject, UseGuards } from '@nestjs/common';
import { Resolver, Query, Mutation, Args, Context } from '@nestjs/graphql';
import { UsersService } from './users.service';
import RegisterDto from 'src/users/dtos/register.dto';
import LoginDto from 'src/users/dtos/login.dto';
import {
  NewAvatarObject,
  User,
  UserDocument,
  UserObject,
} from './entities/user.schema';
import { AuthGuard } from 'src/chat/auth.guard';

/* eslint-disable */
@Resolver()
export class UsersResolver {
  @Inject() usersService: UsersService;

  @Query(() => [UserObject])
  searchUser(@Args('keyword') keyword: string): Promise<Array<UserDocument>> {
    return this.usersService.searchUser(keyword);
  }

  @Query(() => UserObject)
  currentUser(@Args('token') token: string): User {
    return this.usersService.currentUser(token);
  }

  @Mutation((_) => UserObject)
  register(@Args('register') registerDto: RegisterDto): Promise<UserDocument> {
    return this.usersService.register(registerDto);
  }

  @Mutation((_) => UserObject)
  login(@Args('login') loginDto: LoginDto): Promise<UserDocument> {
    return this.usersService.login(loginDto);
  }

  @Mutation((_) => NewAvatarObject)
  @UseGuards(AuthGuard)
  changeAvatar(
    @Args('avatar') avatar: string,
    @Context() user,
  ): Promise<NewAvatarObject> {
    return this.usersService.changeAvatar(avatar, user.user._id);
  }
}
