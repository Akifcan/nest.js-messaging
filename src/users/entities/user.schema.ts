import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ObjectType, Field } from '@nestjs/graphql';
import * as md5 from 'md5';

export type UserDocument = User & Document;
@ObjectType()
@Schema({ timestamps: true })
export class User {
  @Field()
  @Prop({ required: true, type: String, unique: true, lowercase: true })
  email: string;
  @Prop({ required: true, type: String, select: false, lowercase: true })
  password: string;
  @Field()
  @Prop({ required: true, type: String, lowercase: true })
  username: string;
  @Field()
  @Prop({ required: false, default: 'avatars/1.png', lowercase: true })
  avatar: string;
}

@ObjectType()
export class UserObject extends User {
  @Field()
  _id: string;
  @Field()
  jwt: string;
}

@ObjectType()
export class NewAvatarObject {
  @Field()
  success: boolean;
  @Field()
  token: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre<User>('save', function (next) {
  this.password = md5(this.password);
  next();
});
