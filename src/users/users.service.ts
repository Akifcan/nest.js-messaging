import {
  ForbiddenException,
  Injectable,
  Inject,
  UnauthorizedException,
} from '@nestjs/common';
import * as md5 from 'md5';

import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import LoginDto from 'src/users/dtos/login.dto';
import RegisterDto from 'src/users/dtos/register.dto';
import { NewAvatarObject, User, UserDocument } from './entities/user.schema';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UsersService {
  @Inject() private jwtService: JwtService;
  @InjectModel(User.name) private userModel: Model<UserDocument>;

  async register(registerDto: RegisterDto): Promise<UserDocument> {
    if (!(await this.isMailUnique(registerDto.email)))
      throw new ForbiddenException();

    const avatar = `avatars/${Math.floor(Math.random() * 50) + 1}.png`;
    const saveUser = await this.userModel.create({ ...registerDto, avatar });
    saveUser['jwt'] = this.jwtService.sign({ saveUser });
    return saveUser;
  }

  async login(loginDto: LoginDto): Promise<UserDocument> {
    const user = await this.userModel.findOne({
      email: loginDto.email,
      password: md5(loginDto.password),
    });
    if (user) {
      user['jwt'] = this.jwtService.sign({ user });
      return user;
    } else {
      throw new UnauthorizedException();
    }
  }

  currentUser(token: string): User {
    return (this.jwtService.decode(token) as any).user;
  }

  async searchUser(keyword: string): Promise<Array<UserDocument>> {
    const users = await this.userModel.find({
      $or: [{ username: { $regex: keyword } }, { email: { $regex: keyword } }],
    });
    console.log(users);
    return users;
  }

  async changeAvatar(avatar: string, userId: string): Promise<NewAvatarObject> {
    const newAvatar = await this.userModel.updateOne(
      { _id: new Types.ObjectId(userId) },
      {
        avatar: `avatars/${avatar}.png`,
      },
    );
    const user = await this.userModel.findOne({
      _id: new Types.ObjectId(userId),
    });
    const token = this.jwtService.sign({ user });
    console.log(token);
    if (newAvatar.modifiedCount == 1) return { token: token, success: true };
    return { token: token, success: false };
  }

  private async isMailUnique(email: string): Promise<boolean> {
    const findEmail = await this.userModel.findOne({ email });
    if (!findEmail) {
      return true;
    } else {
      return false;
    }
  }
}
