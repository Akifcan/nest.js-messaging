import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { User, UserDocument } from 'src/users/entities/user.schema';
import MessageDto from './dtos/message.dto';
import {
  ConversationDocument,
  Conversation,
} from './entities/conversation.schema';
import LastMessagesType from './entities/lastMessages.type';
import { Message, MessageDocument } from './entities/message.schema';

@Injectable()
export class ChatService {
  @InjectModel(Message.name) private messageModel: Model<MessageDocument>;
  @InjectModel(User.name) private userModel: Model<UserDocument>;
  @InjectModel(Conversation.name)
  private conversationModel: Model<ConversationDocument>;

  async listConversations(
    conversationId: string,
  ): Promise<Array<MessageDocument>> {
    const messages = await this.messageModel.find({
      conversationId: new Types.ObjectId(conversationId),
    });
    return messages;
  }

  async listMessages(userId: string): Promise<[LastMessagesType]> {
    const messages = await this.conversationModel.aggregate([
      {
        $match: { users: userId },
      },
      {
        $sort: { createdAt: -1 },
      },
      {
        $lookup: {
          from: 'messages',
          localField: '_id',
          foreignField: 'conversationId',
          as: 'messages',
        },
      },
    ]);
    const lastMessages = messages.map((item) => {
      return {
        conversationId: item._id as string,
        content: item.messages[item.messages.length - 1].content as string,
        friend: item.messages[item.messages.length - 1].users.find(
          (ids) => ids !== userId,
        ) as string,
      };
    }) as [LastMessagesType];
    for (let message in lastMessages) {
      const { avatar, username, _id } = await this.userModel.findOne({
        _id: new Types.ObjectId(lastMessages[message].friend),
      });

      lastMessages[message]['_id'] = _id;
      lastMessages[message]['avatar'] = avatar;
      lastMessages[message]['username'] = username;
    }
    return lastMessages;
  }

  async sendMessage(
    message: MessageDto,
    sender: string,
  ): Promise<MessageDocument> {
    const conversation = await this.conversationModel.findOne({
      $and: [
        {
          users: message.receiver,
        },
        {
          users: sender,
        },
      ],
    });
    console.log(conversation);
    if (!conversation) {
      const newConversation = await this.conversationModel.create({
        users: [message.receiver, sender],
      });
      return await this.messageModel.create({
        conversationId: newConversation._id,
        sender: new Types.ObjectId(sender),
        users: [message.receiver, sender],
        content: message.content,
      });
    } else {
      return await this.messageModel.create({
        conversationId: conversation._id,
        sender: new Types.ObjectId(sender),
        users: [message.receiver, sender],
        content: message.content,
      });
    }
  }
}
