import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Inject,
  ForbiddenException,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';

interface IJWTResult {
  user: {
    _id: string;
    avatar: string;
    email: string;
    createdAt: string;
    updatedAt: string;
    username: string;
  };
}

@Injectable()
export class AuthGuard implements CanActivate {
  @Inject() jwtService: JwtService;

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const gqlCtx = GqlExecutionContext.create(context);
    const request = gqlCtx.getContext().req;
    if (!request.headers.token) {
      throw new ForbiddenException();
    } else {
      const user: IJWTResult = this.jwtService.decode(
        request.headers.token,
      ) as IJWTResult;
      gqlCtx.getContext().user = user.user;
      return true;
    }
  }
}
