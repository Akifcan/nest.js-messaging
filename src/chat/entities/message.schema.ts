import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';

export type MessageDocument = Message & Document;
@ObjectType()
@Schema({ timestamps: true })
export class Message {
  @Field((returns) => String)
  @Prop({ required: true, type: Types.ObjectId })
  conversationId: Types.ObjectId;
  @Prop({ required: true, type: Array })
  @Field((returns) => [String])
  users: [string];
  @Field()
  @Prop({ required: true, type: String })
  content: string;
  @Field((returns) => String)
  @Prop({ required: true, type: Types.ObjectId })
  sender: Types.ObjectId;
}
@ObjectType()
export class MessageType extends Message {
  @Field((returns) => String)
  _id: Types.ObjectId;
  @Field((returns) => String)
  @Field()
  createdAt: Date;
}
export const MessageSchema = SchemaFactory.createForClass(Message);
