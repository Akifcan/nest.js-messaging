import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { ObjectType, Field, ID } from '@nestjs/graphql';

export type ConversationDocument = Conversation & Document;
@ObjectType()
@Schema({ timestamps: true })
export class Conversation {
  @Prop({ required: true, type: Array })
  users: [String];
}

export const ConversationSchema = SchemaFactory.createForClass(Conversation);
