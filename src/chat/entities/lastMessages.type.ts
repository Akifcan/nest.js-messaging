import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
class LastMessagesType {
  @Field()
  _id: string;
  @Field()
  avatar?: string;
  @Field()
  username?: string;
  @Field()
  conversationId: string;
  @Field()
  content: string;
  @Field()
  friend: string;
}

export default LastMessagesType;
