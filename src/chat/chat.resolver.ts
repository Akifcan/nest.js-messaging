import { Inject, UseGuards } from '@nestjs/common';
import { Resolver, Query, Context, Mutation, Args } from '@nestjs/graphql';
import { AuthGuard } from './auth.guard';
import { ChatService } from './chat.service';
import MessageDto from './dtos/message.dto';
import LastMessagesType from './entities/lastMessages.type';
import { MessageDocument, MessageType } from './entities/message.schema';

/* eslint-disable */
@Resolver()
@UseGuards(AuthGuard)
export class ChatResolver {
  @Inject() chatService: ChatService;

  @Query((_) => [LastMessagesType])
  listMessages(@Context() context) {
    return this.chatService.listMessages(context.user._id);
  }

  @Query((_) => [MessageType])
  listConversations(@Args('conversationId') conversationId: string) {
    return this.chatService.listConversations(conversationId);
  }

  @Mutation((_) => MessageType)
  async sendMessage(
    @Args('message') message: MessageDto,
    @Context() context,
  ): Promise<MessageDocument> {
    return this.chatService.sendMessage(message, context.user._id);
  }
}
