import { IsNotEmpty } from 'class-validator';
import { InputType, Field } from '@nestjs/graphql';
@InputType()
class MessageDto {
  @IsNotEmpty()
  @Field()
  receiver: string;
  @IsNotEmpty()
  @Field()
  content: string;
}

export default MessageDto;
